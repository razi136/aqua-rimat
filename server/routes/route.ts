import { NextFunction, Request, Response } from "express";
import {ResponseDataObject} from "../lib/common/responseDataObject";
/**
 * Constructor
 *
 * @class BaseRoute
 */
export class BaseRoute {

    /**
     * Constructor
     *
     * @class BaseRoute
     * @constructor
     */
    constructor() {

    }

    /**
     * Render a page.
     *
     * @class BaseRoute
     * @method render
     * @param req {Request} The request object.
     * @param res {Response} The response object.
     * @param response {Object} Additional options to append to the view's local scope.
     * @return void
     */
    protected render(res: Response, response?: Object) {
        //render view
        res.status(200).json(ResponseDataObject.create(true, response));
    }

    protected renderError(res: Response, response?: Error) {

        let message = response.message;
        //render view
        res.status(200).json(ResponseDataObject.create(false, {message: message}));
    }
}