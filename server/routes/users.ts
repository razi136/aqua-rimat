import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "./route";
import * as _ from 'lodash';
import {UsersWorker} from "../lib/workers/users";


/**
 * / route
 *
 * @class User
 */
export class UsersRoute extends BaseRoute {

    /**
     * Create the routes.
     *
     * @class UsersRoute
     * @method create
     * @static
     */
    public static create(router: Router) {
        //log
        console.log("[UsersRoute::create] Creating packages route.");

        router.get("/api/:userType(admin|user)/:userId", (req: Request, res: Response, next: NextFunction) => {
            new UsersRoute().getUser(req, res, next);
        });

        router.post("/api/:userType(admin|user)/:userId", (req: Request, res: Response, next: NextFunction) => {
            new UsersRoute().createUser(req, res, next);
        });
    }

    /**
     * Constructor
     *
     * @class IndexRoute
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * The home page route.
     *
     * @class IndexRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @param next
     * @next {NextFunction} Execute the next method.
     */
    private getUser(req: Request, res: Response, next: NextFunction) {

        let userId = _.get(req.params, "userId");
        let userType = _.get(req.params, "userType");

        let user = {
            userId: userId,
            userType: userType
        };
        UsersWorker.printParameters(user);
        UsersWorker.sendToView(user);

        this.render(res, {});
    }

    private createUser(req: Request, res: Response, next: NextFunction) {
        // what should i do here?
        this.render(res, {});
    }


}