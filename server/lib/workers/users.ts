
import {IUser} from "../interfaces/user";
import chatServer from "../../socket.io/chat";
/**
 * Created by razih on 28/2/2017.
 */

let _instance = Symbol();
export class UsersWorker {

    /**
     *
     * @returns {UsersWorker}
     */
    public static get instance() {
        return this[_instance] || (this[_instance] = new UsersWorker())
    }

    private constructor(){}

   public static printParameters(user:IUser):void {
        console.log(user);
   }

   public static sendToView(user:IUser) {

        chatServer.emit('chat message', JSON.stringify(user));
   }

}
