export class ResponseDataObject {
    private status:boolean;
    private body:Object;

    public constructor(status, body) {

        this.status = status;
        this.body = body;
    }

    public static create(status, body) {
        return new ResponseDataObject(status, body);
    }
}