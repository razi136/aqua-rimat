import { Component, OnInit, NgZone } from '@angular/core';
import * as sio from 'socket.io-client';
import {FormControl, FormGroup} from "@angular/forms";
import {Http} from "@angular/http";
import {HttpMethods} from "../../lib/enums/httpMethods";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})

export class ChatComponent implements OnInit {
  msgs = [];
  socket: SocketIOClient.Socket;
  userIds:Array<string>=[];
  userDetailsForm:FormGroup;

  HttpMethods: typeof HttpMethods = HttpMethods;


  constructor(private zone: NgZone,
              private userService: UserService) {
    this.userIds = ["1","2","3","4","5","6","7","8","9","10","11"];
  }

  ngOnInit() {
    this.socket = sio({
      path: '/socket.io/chat'
    });
    this.socket.on('chat message', (msg: string) => {
      this.zone.run(() => {
        this.msgs.push(msg);
      });

    });

    this.userDetailsForm = new FormGroup({
      http: new FormControl(HttpMethods.GET),
      userType: new FormControl('Admin'),
      userId: new FormControl("2")
    });
  }

  send(msg: string) {
    // this.msgs.push(msg);
    this.socket.emit('chat message', msg);
  };

  submitUserDetails() {
    let data = this.userDetailsForm.value;

    let url = ['api', data.userType, data.userId].join('/');
    if(data.http === HttpMethods.GET) {
      this.userService.getUser(data.userType, data.userId).subscribe();
    } else {
      this.userService.postUser(data.userType, data.userId).subscribe();
    }
  }

}
