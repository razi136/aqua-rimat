import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  public getUser(userType: string, userId: string) {
    return this.http.get(['api', userType, userId].join("/")).map(res => res.json());
  }

  public postUser(userType: string, userId: string) {
    return this.http.post(['api', userType, userId].join("/"), {}).map(res => res.json());
  }
}
