/**
 * Created by Razi on 30/04/2017.
 */
export interface UserDetails {
    http: string;
    userType: string;
    userId: string;
}